_internal = {
  event_handlers = {},
  key_bindings = {}
}

function register_handler(event, handler)
  if not _internal.event_handlers[event] then
    _internal.event_handlers[event] = {}
  end

  table.insert(_internal.event_handlers[event], handler)
end

function fire_event(event, data)
  if _internal.event_handlers[event] then
    local handled = false

    for i = 1, #_internal.event_handlers[event] do
      local ret = _internal.event_handlers[event][i](data)
      
      if ret then
        handled = true
      end
    end

    return handled
  end

  return true
end

function register_binding(modifiers, key, edge, callback)
  table.insert(_internal.key_bindings, {
    modifiers = modifiers,
    key = key,
    edge = edge,
    callback = callback
  })
end

register_handler(EVENT_KEY_PRESS, function(data)
  local handled = false

  for _, binding in pairs(_internal.key_bindings) do
    if binding.modifiers == data.modifiers and binding.key == data.key then
      handled = true

      if binding.edge == data.pressed then
        binding.callback()
      end
    end
  end

  return handled
end)

register_handler(EVENT_KEY_PRESS, function(data)
  if log_key_press then
    print("EVENT_KEY_PRESS", data.modifiers, data.key, data.pressed)
  end
end)

bill.log(LOG_INFO, "Internal Lua API Loaded")