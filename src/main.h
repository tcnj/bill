#define _POSIX_C_SOURCE 200809L
#include <getopt.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <unistd.h>
#include <string.h>
#include <wayland-server.h>
#include <wlr/backend.h>
#include <wlr/render/wlr_renderer.h>
#include <wlr/types/wlr_cursor.h>
#include <wlr/types/wlr_compositor.h>
#include <wlr/types/wlr_data_device.h>
#include <wlr/types/wlr_input_device.h>
#include <wlr/types/wlr_keyboard.h>
#include <wlr/types/wlr_matrix.h>
#include <wlr/types/wlr_output.h>
#include <wlr/types/wlr_output_layout.h>
#include <wlr/types/wlr_pointer.h>
#include <wlr/types/wlr_seat.h>
#include <wlr/types/wlr_xcursor_manager.h>
#include <wlr/types/wlr_xdg_shell.h>
#include <wlr/types/wlr_layer_shell_v1.h>
#include <wlr/types/wlr_gamma_control.h>
#include <wlr/types/wlr_screencopy_v1.h>
#include <wlr/types/wlr_primary_selection_v1.h>
#include <wlr/types/wlr_idle.h>
#include <wlr/util/edges.h>
#include <wlr/xwayland.h>
#include <wlr/util/log.h>
#include <xkbcommon/xkbcommon.h>
#include <lua.h>
#include <lauxlib.h>
#include <lualib.h>
#include <uuid/uuid.h>

#include <config_lua_embedded.h>

struct bill_server {
  struct wl_display *wl_display;

  struct wlr_backend *wlr_backend;
  struct wlr_renderer *wlr_renderer;

  struct wlr_output_layout *wlr_output_layout;

  struct wlr_compositor *wlr_compositor;

  struct wl_list panels;
  struct wl_list keyboards;
  struct wl_list outputs;

  struct wlr_xdg_shell *wlr_xdg_shell;
  struct wlr_layer_shell_v1 *wlr_layer_shell;
  struct wlr_xwayland *wlr_xwayland;

  struct wlr_cursor *wlr_cursor;
  struct wlr_xcursor_manager *wlr_xcursor_manager;

  struct wlr_seat *wlr_seat;

  struct wl_listener cursor_motion, cursor_motion_absolute, cursor_button, cursor_axis, cursor_frame;

  struct wl_listener new_xdg_surface;
  struct wl_listener new_layer_surface;
  struct wl_listener new_xwayland_surface;

  struct wl_listener new_input;

  struct wl_listener new_output;

  struct lua_State *lua_state;
};

static struct bill_server *server_ref;

enum bill_panel_surface_type {
  BILL_PANEL_SURFACE_UNDEFINED,
  BILL_PANEL_SURFACE_XDG,
  BILL_PANEL_SURFACE_LAYER,
  BILL_PANEL_SURFACE_XWAYLAND
};

struct bill_panel {
  struct wl_list link;

  struct bill_server *server;

  uuid_t uuid;

  enum bill_panel_surface_type surface_type;
  union {
    struct wlr_xdg_surface *wlr_xdg_surface;
    struct wlr_layer_surface_v1 *wlr_layer_surface;
    struct wlr_xwayland_surface *wlr_xwayland_surface;
  };

  struct wl_listener map;
  struct wl_listener unmap;
  struct wl_listener destroy;

  bool mapped;

  int x, y;
  bool hidden;
};

struct bill_keyboard {
  struct wl_list link;

  struct bill_server *server;

  struct wlr_input_device *device;

  struct wl_listener modifiers, key;
};

struct bill_output {
  struct wl_list link;

  struct bill_server *server;

  struct wlr_output *wlr_output;

  struct wl_listener frame;
};

struct render_data {
  struct wlr_output *wlr_output;
  struct wlr_renderer *wlr_renderer;
  struct bill_panel *panel;
  struct timespec *when;
};

#define EVENT_NEW_PANEL 0
#define EVENT_KEY_PRESS 1
#define EVENT_PANEL_FOCUS 2
#define EVENT_SERVER_LOADED 3
#define EVENT_PANEL_DESTROY 4

const struct {
  char *name;
  int value;
} lua_enums[] = {
  {"LOG_DEBUG", WLR_DEBUG},
  {"LOG_INFO", WLR_INFO},
  {"LOG_ERROR", WLR_ERROR},

  {"EVENT_NEW_PANEL", EVENT_NEW_PANEL},
  {"EVENT_KEY_PRESS", EVENT_KEY_PRESS},
  {"EVENT_PANEL_FOCUS", EVENT_PANEL_FOCUS},
  {"EVENT_SERVER_LOADED", EVENT_SERVER_LOADED},
  {"EVENT_PANEL_DESTROY", EVENT_PANEL_DESTROY},

  {"KEY_MOD_SHIFT", WLR_MODIFIER_SHIFT},
  {"KEY_MOD_CAPS", WLR_MODIFIER_CAPS},
  {"KEY_MOD_CTRL", WLR_MODIFIER_CTRL},

  {"KEY_MOD_ALT", WLR_MODIFIER_ALT},
  {"KEY_MOD_LOGO", WLR_MODIFIER_LOGO},

  {"KEY_MOD_1", WLR_MODIFIER_ALT},
  {"KEY_MOD_2", WLR_MODIFIER_MOD2},
  {"KEY_MOD_3", WLR_MODIFIER_MOD3},
  {"KEY_MOD_4", WLR_MODIFIER_LOGO},
  {"KEY_MOD_5", WLR_MODIFIER_MOD5},

  {"BILL_PANEL_SURFACE_UNDEFINED", BILL_PANEL_SURFACE_UNDEFINED},
  {"BILL_PANEL_SURFACE_XDG", BILL_PANEL_SURFACE_XDG},
  {"BILL_PANEL_SURFACE_LAYER", BILL_PANEL_SURFACE_LAYER},
  {"BILL_PANEL_SURFACE_XWAYLAND", BILL_PANEL_SURFACE_XWAYLAND},

  {NULL, 0}
};

static void handle_new_xdg_surface(struct wl_listener *listener, void *data);
static void bill_panel_handle_xdg_surface_map(struct wl_listener *listener, void *data);
static void bill_panel_handle_xdg_surface_unmap(struct wl_listener *listener, void *data);
static void bill_panel_handle_xdg_surface_destroy(struct wl_listener *listener, void *data);

static void handle_new_layer_surface(struct wl_listener *listener, void *data);
static void bill_panel_handle_layer_surface_map(struct wl_listener *listener, void *data);
static void bill_panel_handle_layer_surface_unmap(struct wl_listener *listener, void *data);
static void bill_panel_handle_layer_surface_destroy(struct wl_listener *listener, void *data);

static void handle_new_xwayland_surface(struct wl_listener *listener, void *data);
static void bill_panel_handle_xwayland_surface_map(struct wl_listener *listener, void *data);
static void bill_panel_handle_xwayland_surface_unmap(struct wl_listener *listener, void *data);
static void bill_panel_handle_xwayland_surface_destroy(struct wl_listener *listener, void *data);

static void handle_new_output(struct wl_listener *listener, void *data);
static void handle_cursor_motion(struct wl_listener *listener, void *data);
static void handle_cursor_motion_absolute(struct wl_listener *listener, void *data);
static void handle_cursor_button(struct wl_listener *listener, void *data);
static void handle_cursor_axis(struct wl_listener *listener, void *data);
static void handle_cursor_frame(struct wl_listener *listener, void *data);
static void process_cursor_motion(struct bill_server *server, uint32_t time);
static void process_cursor_button(struct bill_server *server, struct wlr_event_pointer_button *event);
static void bill_output_handle_wlr_output_frame(struct wl_listener *listener, void *data);
static void render_surface(struct wlr_surface *surface, int sx, int sy, void *data);
static void bill_panel_focus_keyboard(struct bill_panel *panel, struct wlr_surface *surface);
static struct bill_panel *find_panel_at(struct bill_server *server, double lx, double ly, struct wlr_surface **surface, double *sx, double *sy);
static bool is_panel_at(struct bill_panel *panel, double lx, double ly, struct wlr_surface **surface, double *sx, double *sy);
static void handle_new_input(struct wl_listener *listener, void *data);
static void handle_new_keyboard(struct bill_server *server, struct wlr_input_device *device);
static void bill_keyboard_handle_modifiers(struct wl_listener *listener, void *data);

static void bill_keyboard_handle_key(struct wl_listener *listener, void *data);
static bool handle_keybinding(struct bill_server *server, uint32_t modifiers, xkb_keysym_t sym, enum wlr_key_state state);
static void handle_new_pointer(struct bill_server *server, struct wlr_input_device *device);

static void setup_lua_context(struct lua_State *L);
static void init_config_enums(struct lua_State *L);
static void init_config_api(struct lua_State *L);
static void init_config_utils(struct lua_State *L);

static void push_lua_keycode_info(struct bill_server *server, uint32_t modifiers, xkb_keysym_t sym, bool pressed);
static void push_lua_panel(struct lua_State *L, struct bill_panel *panel);

static bool fire_lua_event(struct bill_server *server, int event_id);

static int config_log(struct lua_State *L);

static int config_list_panels(struct lua_State *L);
static int config_fork_exec(struct lua_State *L);
static int config_log_level(struct lua_State *L);

static const struct luaL_Reg bill_lualib [] = {
  {"list_panels", config_list_panels},
  {"fork_exec", config_fork_exec},
  {"set_log_level", config_log_level},
  {"log", config_log},

  {NULL, NULL}
};

static int config_panel_tostring(struct lua_State *L);
static int config_panel_info(struct lua_State *L);
static int config_panel_setpos(struct lua_State *L);
static int config_panel_getpos(struct lua_State *L);
static int config_panel_request_size(struct lua_State *L);
static int config_panel_set_hidden(struct lua_State *L);

static const struct luaL_Reg panel_lualib [] = {
  {"__tostring", config_panel_tostring},
  {"info", config_panel_info},
  {"setpos", config_panel_setpos},
  {"getpos", config_panel_getpos},
  {"request_size", config_panel_request_size},
  {"set_hidden", config_panel_set_hidden},

  {NULL, NULL}
};