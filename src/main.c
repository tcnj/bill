#include "main.h"

// XDG

static void handle_new_xdg_surface(struct wl_listener *listener, void *data) {
  struct bill_server *server = wl_container_of(listener, server, new_xdg_surface);
  
  struct wlr_xdg_surface *xdg_surface = data;
  if (xdg_surface->role != WLR_XDG_SURFACE_ROLE_TOPLEVEL) {
    return;
  }

  struct bill_panel *panel = calloc(1, sizeof(struct bill_panel));
  uuid_generate_random(panel->uuid);

  panel->server = server;
  panel->surface_type = BILL_PANEL_SURFACE_XDG;
  panel->wlr_xdg_surface = xdg_surface;

  panel->map.notify = bill_panel_handle_xdg_surface_map;
  wl_signal_add(&xdg_surface->events.map, &panel->map);
  panel->unmap.notify = bill_panel_handle_xdg_surface_unmap;
  wl_signal_add(&xdg_surface->events.unmap, &panel->unmap);
  panel->destroy.notify = bill_panel_handle_xdg_surface_destroy;
  wl_signal_add(&xdg_surface->events.destroy, &panel->destroy);

  // Could add floating moving stuff here

  wl_list_insert(&server->panels, &panel->link);

  wlr_xdg_toplevel_set_tiled(xdg_surface, WLR_EDGE_RIGHT | WLR_EDGE_BOTTOM); // TODO: Move to Lua API

  push_lua_panel(server->lua_state, panel);
  fire_lua_event(server, EVENT_NEW_PANEL);
}

static void bill_panel_handle_xdg_surface_map(struct wl_listener *listener, void *data) {
  /* Called when the surface is mapped, or ready to display on-screen. */
  struct bill_panel *panel = wl_container_of(listener, panel, map);
  panel->mapped = true;
  bill_panel_focus_keyboard(panel, panel->wlr_xdg_surface->surface);
}

static void bill_panel_handle_xdg_surface_unmap(struct wl_listener *listener, void *data) {
  /* Called when the surface is unmapped, and should no longer be shown. */
  struct bill_panel *panel = wl_container_of(listener, panel, unmap);
  panel->mapped = false;
}

static void bill_panel_handle_xdg_surface_destroy(struct wl_listener *listener, void *data) {
  /* Called when the surface is destroyed and should never be shown again. */
  struct bill_panel *panel = wl_container_of(listener, panel, destroy);
  wl_list_remove(&panel->link);

  push_lua_panel(panel->server->lua_state, panel);
  fire_lua_event(panel->server, EVENT_PANEL_DESTROY);

  free(panel);
}

// Layer

static void handle_new_layer_surface(struct wl_listener *listener, void *data) {
  struct bill_server *server = wl_container_of(listener, server, new_layer_surface);
  
  struct wlr_layer_surface_v1 *layer_surface = data;

  struct bill_panel *panel = calloc(1, sizeof(struct bill_panel));
  uuid_generate_random(panel->uuid);

  panel->server = server;
  panel->surface_type = BILL_PANEL_SURFACE_LAYER;
  panel->wlr_layer_surface = layer_surface;

  panel->map.notify = bill_panel_handle_layer_surface_map;
  wl_signal_add(&layer_surface->events.map, &panel->map);
  panel->unmap.notify = bill_panel_handle_layer_surface_unmap;
  wl_signal_add(&layer_surface->events.unmap, &panel->unmap);
  panel->destroy.notify = bill_panel_handle_layer_surface_destroy;
  wl_signal_add(&layer_surface->events.destroy, &panel->destroy);

  // Need to figure out how I'm gonna call wlr_layer_surface_v1_configure()

  wl_list_insert(&server->panels, &panel->link);

  push_lua_panel(server->lua_state, panel);
  fire_lua_event(server, EVENT_NEW_PANEL);
}

static void bill_panel_handle_layer_surface_map(struct wl_listener *listener, void *data) {
  /* Called when the surface is mapped, or ready to display on-screen. */
  struct bill_panel *panel = wl_container_of(listener, panel, map);
  panel->mapped = true;
  bill_panel_focus_keyboard(panel, panel->wlr_layer_surface->surface);
}

static void bill_panel_handle_layer_surface_unmap(struct wl_listener *listener, void *data) {
  /* Called when the surface is unmapped, and should no longer be shown. */
  struct bill_panel *panel = wl_container_of(listener, panel, unmap);
  panel->mapped = false;
}

static void bill_panel_handle_layer_surface_destroy(struct wl_listener *listener, void *data) {
  /* Called when the surface is destroyed and should never be shown again. */
  struct bill_panel *panel = wl_container_of(listener, panel, destroy);
  wl_list_remove(&panel->link);

  push_lua_panel(panel->server->lua_state, panel);
  fire_lua_event(panel->server, EVENT_PANEL_DESTROY);

  free(panel);
}

// XWayland

static void handle_new_xwayland_surface(struct wl_listener *listener, void *data) {
  struct bill_server *server = wl_container_of(listener, server, new_xwayland_surface);
  
  struct wlr_xwayland_surface *xwayland_surface = data;

  struct bill_panel *panel = calloc(1, sizeof(struct bill_panel));
  uuid_generate_random(panel->uuid);

  panel->server = server;
  panel->surface_type = BILL_PANEL_SURFACE_XWAYLAND;
  panel->wlr_xwayland_surface = xwayland_surface;

  panel->map.notify = bill_panel_handle_xwayland_surface_map;
  wl_signal_add(&xwayland_surface->events.map, &panel->map);
  panel->unmap.notify = bill_panel_handle_xwayland_surface_unmap;
  wl_signal_add(&xwayland_surface->events.unmap, &panel->unmap);
  panel->destroy.notify = bill_panel_handle_xwayland_surface_destroy;
  wl_signal_add(&xwayland_surface->events.destroy, &panel->destroy);

  wl_list_insert(&server->panels, &panel->link);

  push_lua_panel(server->lua_state, panel);
  fire_lua_event(server, EVENT_NEW_PANEL);
}

static void bill_panel_handle_xwayland_surface_map(struct wl_listener *listener, void *data) {
  /* Called when the surface is mapped, or ready to display on-screen. */
  struct bill_panel *panel = wl_container_of(listener, panel, map);
  panel->mapped = true;
  bill_panel_focus_keyboard(panel, panel->wlr_xwayland_surface->surface);
}

static void bill_panel_handle_xwayland_surface_unmap(struct wl_listener *listener, void *data) {
  /* Called when the surface is unmapped, and should no longer be shown. */
  struct bill_panel *panel = wl_container_of(listener, panel, unmap);
  panel->mapped = false;
}

static void bill_panel_handle_xwayland_surface_destroy(struct wl_listener *listener, void *data) {
  /* Called when the surface is destroyed and should never be shown again. */
  struct bill_panel *panel = wl_container_of(listener, panel, destroy);
  wl_list_remove(&panel->link);

  push_lua_panel(panel->server->lua_state, panel);
  fire_lua_event(panel->server, EVENT_PANEL_DESTROY);

  free(panel);
}

static void handle_new_output(struct wl_listener *listener, void *data) {
  struct bill_server *server = wl_container_of(listener, server, new_output);
  
  struct wlr_output *output = data;

  if (!wl_list_empty(&output->modes)) {
    struct wlr_output_mode *mode = wl_container_of(output->modes.prev, mode, link);
    wlr_output_set_mode(output, mode);
  }

  struct bill_output *server_output = calloc(1, sizeof(struct bill_output));
  server_output->wlr_output = output;
  server_output->server = server;

  server_output->frame.notify = bill_output_handle_wlr_output_frame;
  wl_signal_add(&output->events.frame, &server_output->frame);
  wl_list_insert(&server->outputs, &server_output->link);

  wlr_output_layout_add_auto(server->wlr_output_layout, output);  // TODO: Allow more advanced layout config

  wlr_output_create_global(output);
}

static void handle_cursor_motion(struct wl_listener *listener, void *data) {
  struct bill_server *server = wl_container_of(listener, server, cursor_motion);

  struct wlr_event_pointer_motion *event = data;

  wlr_cursor_move(server->wlr_cursor, event->device, event->delta_x, event->delta_y);
  process_cursor_motion(server, event->time_msec);
}

static void handle_cursor_motion_absolute(struct wl_listener *listener, void *data) {
  struct bill_server *server = wl_container_of(listener, server, cursor_motion_absolute);

  struct wlr_event_pointer_motion_absolute *event = data;

  wlr_cursor_warp_absolute(server->wlr_cursor, event->device, event->x, event->y);
  process_cursor_motion(server, event->time_msec);
}

static void handle_cursor_button(struct wl_listener *listener, void *data) {
  struct bill_server *server = wl_container_of(listener, server, cursor_button);

  struct wlr_event_pointer_button *event = data;

  process_cursor_button(server, event);
}

static void handle_cursor_axis(struct wl_listener *listener, void *data) {
  struct bill_server *server = wl_container_of(listener, server, cursor_axis);

  struct wlr_event_pointer_axis *event = data;

  wlr_seat_pointer_notify_axis(server->wlr_seat, event->time_msec, event->orientation, event->delta, event->delta_discrete, event->source);
}

static void handle_cursor_frame(struct wl_listener *listener, void *data) {
  struct bill_server *server = wl_container_of(listener, server, cursor_frame);

  wlr_seat_pointer_notify_frame(server->wlr_seat);
}

static void process_cursor_motion(struct bill_server *server, uint32_t time) {
  // TODO: Cleanup
  double sx, sy;

  struct wlr_seat *seat = server->wlr_seat;
  struct wlr_surface *surface = NULL;

  struct bill_panel *panel = find_panel_at(server, server->wlr_cursor->x, server->wlr_cursor->y, &surface, &sx, &sy);

  if (!panel) {
    /* If there's no panel under the cursor, set the cursor image to a
     * default. This is what makes the cursor image appear when you move it
     * around the screen, not over any panels. */
    wlr_xcursor_manager_set_cursor_image(server->wlr_xcursor_manager, "left_ptr", server->wlr_cursor);
  }

  if (surface) {
    bool focus_changed = seat->pointer_state.focused_surface != surface;
    /*
     * "Enter" the surface if necessary. This lets the client know that the
     * cursor has entered one of its surfaces.
     *
     * Note that this gives the surface "pointer focus", which is distinct
     * from keyboard focus. You get pointer focus by moving the pointer over
     * a window.
     */
    wlr_seat_pointer_notify_enter(seat, surface, sx, sy);
    if (!focus_changed) {
      /* The enter event contains coordinates, so we only need to notify
       * on motion if the focus did not change. */
      wlr_seat_pointer_notify_motion(seat, time, sx, sy);
    }
  } else {
    /* Clear pointer focus so future button events and such are not sent to
     * the last client to have the cursor over it. */
    wlr_seat_pointer_clear_focus(seat);
  }
}

static void process_cursor_button(struct bill_server *server, struct wlr_event_pointer_button *event) {
  wlr_seat_pointer_notify_button(server->wlr_seat, event->time_msec, event->button, event->state);

  double sx, sy;

  // struct wlr_seat *seat = server->wlr_seat;
  struct wlr_surface *surface;

  struct bill_panel *panel = find_panel_at(server, server->wlr_cursor->x, server->wlr_cursor->y, &surface, &sx, &sy);
  
  if (event->state == WLR_BUTTON_PRESSED) {
    bill_panel_focus_keyboard(panel, surface);
  }
}

static void bill_output_handle_wlr_output_frame(struct wl_listener *listener, void *data) {
  struct bill_output *output = wl_container_of(listener, output, frame);
  struct wlr_renderer *renderer = output->server->wlr_renderer;

  // TODO: Cleanup

  struct timespec now;
  clock_gettime(CLOCK_MONOTONIC, &now);

  /* wlr_output_attach_render makes the OpenGL context current. */
  if (!wlr_output_attach_render(output->wlr_output, NULL)) {
    return;
  }

  /* The "effective" resolution can change if you rotate your outputs. */
  int width, height;
  wlr_output_effective_resolution(output->wlr_output, &width, &height);
  /* Begin the renderer (calls glViewport and some other GL sanity checks) */
  wlr_renderer_begin(renderer, width, height);

  float color[4] = {0.3, 0.3, 0.3, 1.0}; // TODO: Config. Background stuff?
  wlr_renderer_clear(renderer, color);

  /* Each subsequent window we render is rendered on top of the last. Because
   * our panel list is ordered front-to-back, we iterate over it backwards. */
  struct bill_panel *panel;
  wl_list_for_each_reverse(panel, &output->server->panels, link) {
    if (!panel->mapped || panel->hidden) {
      /* An unmapped panel should not be rendered. */
      continue;
    }
    struct render_data rdata = {
      .wlr_output = output->wlr_output,
      .panel = panel,
      .wlr_renderer = renderer,
      .when = &now,
    };
    /* This calls our render_surface function for each surface among the
     * xdg_surface's toplevel and popups. */
    switch(panel->surface_type){
      case BILL_PANEL_SURFACE_XDG:
        wlr_xdg_surface_for_each_surface(panel->wlr_xdg_surface, render_surface, &rdata);
        break;
      case BILL_PANEL_SURFACE_LAYER:
        wlr_layer_surface_v1_for_each_surface(panel->wlr_layer_surface, render_surface, &rdata);
        break;
      case BILL_PANEL_SURFACE_XWAYLAND:
        render_surface(panel->wlr_xwayland_surface->surface, 0, 0, &rdata);
        break;
      default:
        wlr_log(WLR_ERROR, "Unknown panel surface type!");
        break;
    }
  }

  /* Hardware cursors are rendered by the GPU on a separate plane, and can be
   * moved around without re-rendering what's beneath them - which is more
   * efficient. However, not all hardware supports hardware cursors. For this
   * reason, wlroots provides a software fallback, which we ask it to render
   * here. wlr_cursor handles configuring hardware vs software cursors for you,
   * and this function is a no-op when hardware cursors are in use. */
  wlr_output_render_software_cursors(output->wlr_output, NULL);

  /* Conclude rendering and swap the buffers, showing the final frame
   * on-screen. */
  wlr_renderer_end(renderer);
  wlr_output_commit(output->wlr_output);
}

static void render_surface(struct wlr_surface *surface, int sx, int sy, void *data) {
  // TODO: Cleanup
  /* This function is called for every surface that needs to be rendered. */
  struct render_data *rdata = data;
  struct bill_panel *panel = rdata->panel;
  struct wlr_output *output = rdata->wlr_output;

  /* We first obtain a wlr_texture, which is a GPU resource. wlroots
   * automatically handles negotiating these with the client. The underlying
   * resource could be an opaque handle passed from the client, or the client
   * could have sent a pixel buffer which we copied to the GPU, or a few other
   * means. You don't have to worry about this, wlroots takes care of it. */
  struct wlr_texture *texture = wlr_surface_get_texture(surface);
  if (texture == NULL) {
    return;
  }

  /* The panel has a position in layout coordinates. If you have two displays,
   * one next to the other, both 1080p, a panel on the rightmost display might
   * have layout coordinates of 2000,100. We need to translate that to
   * output-local coordinates, or (2000 - 1920). */
  double ox = 0, oy = 0;
  wlr_output_layout_output_coords(panel->server->wlr_output_layout, output, &ox, &oy);
  ox += panel->x + sx, oy += panel->y + sy;

  /* We also have to apply the scale factor for HiDPI outputs. This is only
   * part of the puzzle, TinyWL does not fully support HiDPI. */
  struct wlr_box box = {
    .x = ox * output->scale,
    .y = oy * output->scale,
    .width = surface->current.width * output->scale,
    .height = surface->current.height * output->scale,
  };

  /*
   * Those familiar with OpenGL are also familiar with the role of matricies
   * in graphics programming. We need to prepare a matrix to render the panel
   * with. wlr_matrix_project_box is a helper which takes a box with a desired
   * x, y coordinates, width and height, and an output geometry, then
   * prepares an orthographic projection and multiplies the necessary
   * transforms to produce a model-panel-projection matrix.
   *
   * Naturally you can do this any way you like, for example to make a 3D
   * compositor.
   */
  float matrix[9];
  enum wl_output_transform transform = wlr_output_transform_invert(surface->current.transform);
  wlr_matrix_project_box(matrix, &box, transform, 0, output->transform_matrix);

  /* This takes our matrix, the texture, and an alpha, and performs the actual
   * rendering on the GPU. */
  wlr_render_texture_with_matrix(rdata->wlr_renderer, texture, matrix, 1);

  /* This lets the client know that we've displayed that frame and it can
   * prepare another one now if it likes. */
  wlr_surface_send_frame_done(surface, rdata->when);
}

static void bill_panel_focus_keyboard(struct bill_panel *panel, struct wlr_surface *surface) {
  // TODO: tidy up
  if (panel == NULL) {
    return;
  }

  struct bill_server *server = panel->server;
  struct wlr_seat *seat = server->wlr_seat;

  struct wlr_surface *prev_surface = seat->keyboard_state.focused_surface;

  if (prev_surface == surface) {
    /* Don't re-focus an already focused surface. */
    return;
  }

  if (prev_surface) {
    /*
     * Deactivate the previously focused surface. This lets the client know
     * it no longer has focus and the client will repaint accordingly, e.g.
     * stop displaying a caret.
     */
    if (wlr_surface_is_xdg_surface(seat->keyboard_state.focused_surface)) {
      struct wlr_xdg_surface *previous = wlr_xdg_surface_from_wlr_surface(seat->keyboard_state.focused_surface);
      wlr_xdg_toplevel_set_activated(previous, false);
    } else if (wlr_surface_is_xwayland_surface(seat->keyboard_state.focused_surface)) {
      struct wlr_xwayland_surface *previous = wlr_xwayland_surface_from_wlr_surface(seat->keyboard_state.focused_surface);
      wlr_xwayland_surface_activate(previous, false);
    } // TODO: Add clause for layer shell
  }

  struct wlr_keyboard *keyboard = wlr_seat_get_keyboard(seat);
  /* Move the panel to the front */
  // wl_list_remove(&panel->link);
  // wl_list_insert(&server->panels, &panel->link);
  /* Activate the new surface */
  struct wlr_surface *new_surface;
  switch(panel->surface_type){
    case BILL_PANEL_SURFACE_XDG:
      wlr_xdg_toplevel_set_activated(panel->wlr_xdg_surface, true);
      new_surface = panel->wlr_xdg_surface->surface;
      break;
    case BILL_PANEL_SURFACE_LAYER:
      new_surface = panel->wlr_layer_surface->surface;
      break;
    case BILL_PANEL_SURFACE_XWAYLAND:
      wlr_xwayland_surface_activate(panel->wlr_xwayland_surface, true);
      new_surface = panel->wlr_xwayland_surface->surface;
      break;
    default:
      wlr_log(WLR_ERROR, "Unknown panel surface type!");
      break;
  }
  /*
   * Tell the seat to have the keyboard enter this surface. wlroots will keep
   * track of this and automatically send key events to the appropriate
   * clients without additional work on your part.
   */
  wlr_seat_keyboard_notify_enter(seat, new_surface, keyboard->keycodes, keyboard->num_keycodes, &keyboard->modifiers);

  push_lua_panel(server->lua_state, panel);
  fire_lua_event(server, EVENT_PANEL_FOCUS);
}

static struct bill_panel *find_panel_at(struct bill_server *server, double lx, double ly, struct wlr_surface **surface, double *sx, double *sy) {
  /* This iterates over all of our surfaces and attempts to find one under the
   * cursor. This relies on server->panels being ordered from top-to-bottom. */
  struct bill_panel *panel;
  wl_list_for_each(panel, &server->panels, link) {
    if (is_panel_at(panel, lx, ly, surface, sx, sy)) {
      return panel;
    }
  }
  return NULL;
}

static bool is_panel_at(struct bill_panel *panel, double lx, double ly, struct wlr_surface **surface, double *sx, double *sy) {
  // TODO: Cleanup
  /*
   * XDG toplevels may have nested surfaces, such as popup windows for context
   * menus or tooltips. This function tests if any of those are underneath the
   * coordinates lx and ly (in output Layout Coordinates). If so, it sets the
   * surface pointer to that wlr_surface and the sx and sy coordinates to the
   * coordinates relative to that surface's top-left corner.
   */

  if(panel->hidden)
    return false;

  double panel_sx = lx - panel->x;
  double panel_sy = ly - panel->y;

  // struct wlr_surface_state *state = &panel->wlr_xdg_surface->surface->current;

  double _sx, _sy;
  struct wlr_surface *_surface = NULL;

  switch(panel->surface_type){
    case BILL_PANEL_SURFACE_XDG:
    
      _surface = wlr_xdg_surface_surface_at(panel->wlr_xdg_surface, panel_sx, panel_sy, &_sx, &_sy);

      if (_surface != NULL) {
        *sx = _sx;
        *sy = _sy;
        *surface = _surface;
        return true;
      }

      break;
    case BILL_PANEL_SURFACE_LAYER:
    
      _surface = wlr_layer_surface_v1_surface_at(panel->wlr_layer_surface, panel_sx, panel_sy, &_sx, &_sy);

      if (_surface != NULL) {
        *sx = _sx;
        *sy = _sy;
        *surface = _surface;
        return true;
      }

      break;
    case BILL_PANEL_SURFACE_XWAYLAND:

      if (panel_sx >= 0 && panel_sy >= 0 && panel_sx <= panel->wlr_xwayland_surface->width && panel_sy <= panel->wlr_xwayland_surface->height) {
        *sx = panel_sx;
        *sy = panel_sy;
        *surface = panel->wlr_xwayland_surface->surface;
        return true;
      }
      break;
    default:
      wlr_log(WLR_ERROR, "Unknown panel surface type!");
      break;
  }

  return false;
}

static void handle_new_input(struct wl_listener *listener, void *data) {
  struct bill_server *server = wl_container_of(listener, server, new_input);

  struct wlr_input_device *device = data;

  switch (device->type) {
  case WLR_INPUT_DEVICE_KEYBOARD:
    handle_new_keyboard(server, device);
    break;
  case WLR_INPUT_DEVICE_POINTER:
    handle_new_pointer(server, device);
    break;
  default:
    break;
  }

  uint32_t capabilities = WL_SEAT_CAPABILITY_POINTER;

  if (!wl_list_empty(&server->keyboards)) {
    capabilities |= WL_SEAT_CAPABILITY_KEYBOARD;
  }

  wlr_seat_set_capabilities(server->wlr_seat, capabilities);
}

static void handle_new_keyboard(struct bill_server *server, struct wlr_input_device *device) {
  struct bill_keyboard *keyboard = calloc(1, sizeof(struct bill_keyboard));
  keyboard->server = server;
  keyboard->device = device;

  /* We need to prepare an XKB keymap and assign it to the keyboard. This
   * assumes the defaults (e.g. layout = "us"). */
  struct xkb_rule_names rules = { 0 };
  struct xkb_context *context = xkb_context_new(XKB_CONTEXT_NO_FLAGS);
  struct xkb_keymap *keymap = xkb_map_new_from_names(context, &rules, XKB_KEYMAP_COMPILE_NO_FLAGS);

  wlr_keyboard_set_keymap(device->keyboard, keymap);
  xkb_keymap_unref(keymap);
  xkb_context_unref(context);
  wlr_keyboard_set_repeat_info(device->keyboard, 25, 600); // TODO: Configure repeat info

  /* Here we set up listeners for keyboard events. */
  keyboard->modifiers.notify = bill_keyboard_handle_modifiers;
  wl_signal_add(&device->keyboard->events.modifiers, &keyboard->modifiers);
  keyboard->key.notify = bill_keyboard_handle_key;
  wl_signal_add(&device->keyboard->events.key, &keyboard->key);

  wlr_seat_set_keyboard(server->wlr_seat, device);

  /* And add the keyboard to our list of keyboards */
  wl_list_insert(&server->keyboards, &keyboard->link);
}

static void bill_keyboard_handle_modifiers(struct wl_listener *listener, void *data) {
  struct bill_keyboard *keyboard = wl_container_of(listener, keyboard, modifiers);
  /*
   * A seat can only have one keyboard, but this is a limitation of the
   * Wayland protocol - not wlroots. We assign all connected keyboards to the
   * same seat. You can swap out the underlying wlr_keyboard like this and
   * wlr_seat handles this transparently.
   */
  wlr_seat_set_keyboard(keyboard->server->wlr_seat, keyboard->device);
  /* Send modifiers to the client. */
  wlr_seat_keyboard_notify_modifiers(keyboard->server->wlr_seat, &keyboard->device->keyboard->modifiers);
}

static void bill_keyboard_handle_key(struct wl_listener *listener, void *data) {
  struct bill_keyboard *keyboard = wl_container_of(listener, keyboard, key);
  struct bill_server *server = keyboard->server;

  struct wlr_seat *seat = server->wlr_seat;

  struct wlr_event_keyboard_key *event = data;

  // TODO: Cleanup
  /* Translate libinput keycode -> xkbcommon */
  uint32_t keycode = event->keycode + 8;
  /* Get a list of keysyms based on the keymap for this keyboard */
  const xkb_keysym_t *syms;
  int nsyms = xkb_state_key_get_syms(keyboard->device->keyboard->xkb_state, keycode, &syms);

  bool handled = false;

  uint32_t modifiers = wlr_keyboard_get_modifiers(keyboard->device->keyboard);
  for (int i = 0; i < nsyms; i++) {
    handled = handle_keybinding(server, modifiers, syms[i], event->state);
  }

  if (!handled) {
    /* Otherwise, we pass it along to the client. */
    wlr_seat_set_keyboard(seat, keyboard->device);
    wlr_seat_keyboard_notify_key(seat, event->time_msec, event->keycode, event->state);
  }
}

static bool handle_keybinding(struct bill_server *server, uint32_t modifiers, xkb_keysym_t sym, enum wlr_key_state state) {
  // TODO: Handle keybinds
  // Remember to only do stuff on press/release, but always return true if it matches a binding

  push_lua_keycode_info(server, modifiers, sym, state == WLR_KEY_PRESSED);
  bool lua_handled = fire_lua_event(server, EVENT_KEY_PRESS);

  if(lua_handled) {
    wlr_log(WLR_DEBUG, "Keypress handled by lua");
    return true;
  }

  if(modifiers & WLR_MODIFIER_ALT && modifiers & WLR_MODIFIER_SHIFT) {
    if(sym == XKB_KEY_Q) {
      if(state == WLR_KEY_PRESSED)
        wl_display_terminate(server->wl_display);
      return true;
    }
  }

  return false;
}

static void handle_new_pointer(struct bill_server *server, struct wlr_input_device *device) {
  wlr_cursor_attach_input_device(server->wlr_cursor, device); // Yay!
}

static void setup_lua_context(struct lua_State *L) {
  init_config_enums(L);
  init_config_api(L);
  init_config_utils(L);
}

static void init_config_enums(struct lua_State *L) {
  for (int i = 0; lua_enums[i].name != NULL; i++) {
    lua_pushinteger(L, lua_enums[i].value);
    lua_setglobal(L, lua_enums[i].name);
  }
}

static void init_config_api(struct lua_State *L) {
  luaL_newmetatable(L, "bill.panel");

  lua_pushstring(L, "__index");
  lua_pushvalue(L, -2);  /* pushes the metatable */
  lua_settable(L, -3);  /* metatable.__index = metatable */

  luaL_openlib(L, NULL, panel_lualib, 0);

  luaL_openlib(L, "bill", bill_lualib, 0);
}

static void init_config_utils(struct lua_State *L) {
  char *config_src = malloc(config_lua_len + 1);
  memcpy(config_src, config_lua, config_lua_len);
  config_src[config_lua_len] = 0;

  int error = luaL_loadstring(L, config_src) || lua_pcall(L, 0, 0, 0);
  if (error) {
    wlr_log(WLR_ERROR, "Internal LUA Error: %s", lua_tostring(L, -1));
    lua_pop(L, 1);
  }
}

static void push_lua_keycode_info(struct bill_server *server, uint32_t modifiers, xkb_keysym_t sym, bool pressed) {
  struct lua_State *L = server->lua_state;

  char keyname_buf[128];
  int len = xkb_keysym_get_name(xkb_keysym_to_upper(sym), keyname_buf, 256);

  lua_newtable(L);

  lua_pushstring(L, "modifiers");
  lua_pushinteger(L, modifiers);
  lua_settable(L, -3);

  lua_pushstring(L, "key");
  lua_pushlstring(L, keyname_buf, len);
  lua_settable(L, -3);

  lua_pushstring(L, "pressed");
  lua_pushboolean(L, pressed);
  lua_settable(L, -3);
}

static void push_lua_panel(struct lua_State *L, struct bill_panel *panel) { // TODO: Memoize with Lua registry to allow comparisons etc.
  char *uuid = lua_newuserdata(L, 37);
  uuid_unparse_lower(panel->uuid, uuid);

  luaL_getmetatable(L, "bill.panel");
  lua_setmetatable(L, -2);
}

static struct bill_panel *check_lua_panel(struct lua_State *L) {
  void *ud = luaL_checkudata(L, 1, "bill.panel");
  luaL_argcheck(L, ud != NULL, 1, "`panel' expected");
  char *uuid = (char *) ud;

  struct bill_panel *panel;
  wl_list_for_each_reverse(panel, &server_ref->panels, link) {
    char *uuid_buffer = malloc(37);
    uuid_unparse_lower(panel->uuid, uuid_buffer);

    if(strcmp(uuid_buffer, uuid) == 0) {
      free(uuid_buffer);
      return panel;
    }

    free(uuid_buffer);
  }

  luaL_argerror(L, 1, "Panel has been deleted"); // TODO: Is this the best solution?

  return NULL;
}

static bool fire_lua_event(struct bill_server *server, int event_id) {
  struct lua_State *L = server->lua_state;

  if (lua_gettop(L) < 1)
    lua_pushnil(L);

  lua_getglobal(L, "fire_event");
  lua_pushinteger(L, event_id);
  lua_pushvalue(L, -3);

  int error = lua_pcall(L, 2, 1, 0);
  if (error) {
    wlr_log(WLR_ERROR, "Error firing lua event: %s", lua_tostring(L, -1));
    lua_pop(L, 2);
    return false;
  }
  bool handled = lua_toboolean(L, -1);
  lua_pop(L, 2);
  return handled;
}

static int config_log(struct lua_State *L) {
  int log_level = luaL_checkinteger(L, 1);
  const char *log_msg = luaL_checkstring(L, 2);

  if (log_msg != NULL)
    wlr_log(log_level, "[LUA] %s", log_msg);
  
  return 0;
}

static int config_list_panels(struct lua_State *L) {
  lua_newtable(L);

  int i = 1;
  struct bill_panel *panel;
  wl_list_for_each_reverse(panel, &server_ref->panels, link) {
    lua_pushinteger(L, i);
    push_lua_panel(L, panel);
    lua_settable(L, -3);

    i++;
  }

  return 1;
}

static int config_fork_exec(struct lua_State *L) {
  const char* command = luaL_checkstring(L, 1);

  if(fork() == 0)
    execl("/bin/sh", "/bin/sh", "-c", command, (void *) NULL);

  return 0;
}

static int config_log_level(struct lua_State *L) {
  uint32_t level = luaL_checkinteger(L, 1);
  luaL_argcheck(L, level < WLR_LOG_IMPORTANCE_LAST, 1, "valid log level expected");

  wlr_log_init(level, NULL);

  return 0;
}

static int config_panel_tostring(struct lua_State *L) {
  void *ud = luaL_checkudata(L, 1, "bill.panel");
  luaL_argcheck(L, ud != NULL, 1, "`panel' expected");
  char *uuid = (char *) ud;

  lua_pushstring(L, uuid);
  return 1;
}

static int config_panel_info(struct lua_State *L) {
  struct bill_panel *panel = check_lua_panel(L);

  lua_newtable(L);

  lua_pushstring(L, "x"); lua_pushinteger(L, panel->x); lua_settable(L, -3);
  lua_pushstring(L, "y"); lua_pushinteger(L, panel->y); lua_settable(L, -3);

  lua_pushstring(L, "surface_type"); lua_pushinteger(L, panel->surface_type); lua_settable(L, -3);

  lua_pushstring(L, "mapped"); lua_pushboolean(L, panel->mapped); lua_settable(L, -3);
  lua_pushstring(L, "hidden"); lua_pushboolean(L, panel->hidden); lua_settable(L, -3);

  switch(panel->surface_type) {
    case BILL_PANEL_SURFACE_XDG:
      lua_pushstring(L, "label"); lua_pushstring(L, panel->wlr_xdg_surface->toplevel->app_id); lua_settable(L, -3);

      lua_pushstring(L, "title"); lua_pushstring(L, panel->wlr_xdg_surface->toplevel->title); lua_settable(L, -3);
      lua_pushstring(L, "app_id"); lua_pushstring(L, panel->wlr_xdg_surface->toplevel->app_id); lua_settable(L, -3);
      break;
    case BILL_PANEL_SURFACE_LAYER:
      lua_pushstring(L, "label"); lua_pushstring(L, panel->wlr_layer_surface->namespace); lua_settable(L, -3);

      lua_pushstring(L, "namespace"); lua_pushstring(L, panel->wlr_layer_surface->namespace); lua_settable(L, -3);
      break;
    case BILL_PANEL_SURFACE_XWAYLAND:
      lua_pushstring(L, "label"); lua_pushstring(L, panel->wlr_xwayland_surface->instance); lua_settable(L, -3);

      lua_pushstring(L, "title"); lua_pushstring(L, panel->wlr_xwayland_surface->title); lua_settable(L, -3);
      lua_pushstring(L, "class"); lua_pushstring(L, panel->wlr_xwayland_surface->class); lua_settable(L, -3);
      lua_pushstring(L, "instance"); lua_pushstring(L, panel->wlr_xwayland_surface->instance); lua_settable(L, -3);
      lua_pushstring(L, "role"); lua_pushstring(L, panel->wlr_xwayland_surface->role); lua_settable(L, -3);
      break;
    default:
      break;
  }

  return 1;
}

static int config_panel_setpos(struct lua_State *L) {
  struct bill_panel *panel = check_lua_panel(L);
  int x = luaL_checkinteger(L, 2);
  int y = luaL_checkinteger(L, 3);

  panel->x = x;
  panel->y = y;

  return 0;
}

static int config_panel_getpos(struct lua_State *L) {
  struct bill_panel *panel = check_lua_panel(L);

  lua_pushinteger(L, panel->x);
  lua_pushinteger(L, panel->y);

  return 2;
}

static int config_panel_request_size(struct lua_State *L) {
  struct bill_panel *panel = check_lua_panel(L);
  int width = luaL_checkinteger(L, 2);
  int height = luaL_checkinteger(L, 3);

  switch (panel->surface_type) {
  case BILL_PANEL_SURFACE_XDG:
    wlr_xdg_toplevel_set_size(panel->wlr_xdg_surface, width, height);
    break;
  case BILL_PANEL_SURFACE_LAYER:
    wlr_layer_surface_v1_configure(panel->wlr_layer_surface, width, height);
    break;
  case BILL_PANEL_SURFACE_XWAYLAND:
    wlr_xwayland_surface_configure(panel->wlr_xwayland_surface, panel->x, panel->y, width, height);
    break;
  default:
    break;
  }

  return 0;
}

static int config_panel_set_hidden(struct lua_State *L) {
  struct bill_panel *panel = check_lua_panel(L);
  
  luaL_argcheck(L, lua_isboolean(L, 2), 2, "boolean expected");
  bool hidden = lua_toboolean(L, 2);

  panel->hidden = hidden;

  if(hidden)
    wlr_seat_keyboard_clear_focus(server_ref->wlr_seat);

  return 0;
}

int main(int argc, char **argv) {
  struct bill_server server;
  server_ref = &server;

  wlr_log_init(WLR_DEBUG, NULL);

  char c;
  char *configFile;
  configFile = "~/.bill.lua";

  while ((c = getopt(argc, argv, "c:")) != -1)
    switch (c) {
    case 'c':
      configFile = optarg;
      break;
    case ':':
      wlr_log(WLR_ERROR, "Option -%c requires an operand", optopt);
      return 2;
      break;
    case '?':
      wlr_log(WLR_ERROR, "Unknown option %c", optopt);
      return 2;
      break;
    }

  wl_list_init(&server.panels);

  server.lua_state = lua_open();
  luaL_openlibs(server.lua_state);

  setup_lua_context(server.lua_state);

  luaL_loadfile(server.lua_state, configFile);
  int error = lua_pcall(server.lua_state, 0, 0, 0);
  if (error) {
    wlr_log(WLR_ERROR, "LUA Error: %s", lua_tostring(server.lua_state, -1));
    lua_pop(server.lua_state, 1);
    return 3;
  }

  server.wl_display = wl_display_create();

  server.wlr_backend = wlr_backend_autocreate(server.wl_display, NULL);

  server.wlr_renderer = wlr_backend_get_renderer(server.wlr_backend);
  wlr_renderer_init_wl_display(server.wlr_renderer, server.wl_display);

  server.wlr_compositor = wlr_compositor_create(server.wl_display, server.wlr_renderer);
  wlr_data_device_manager_create(server.wl_display); // returns

  server.wlr_output_layout = wlr_output_layout_create();

  wl_list_init(&server.outputs);
  server.new_output.notify = handle_new_output;
  wl_signal_add(&server.wlr_backend->events.new_output, &server.new_output);

  server.wlr_layer_shell = wlr_layer_shell_v1_create(server.wl_display);
  server.new_layer_surface.notify = handle_new_layer_surface;
  wl_signal_add(&server.wlr_layer_shell->events.new_surface, &server.new_layer_surface);

  server.wlr_xdg_shell = wlr_xdg_shell_create(server.wl_display);
  server.new_xdg_surface.notify = handle_new_xdg_surface;
  wl_signal_add(&server.wlr_xdg_shell->events.new_surface, &server.new_xdg_surface);

  server.wlr_xwayland = wlr_xwayland_create(server.wl_display, server.wlr_compositor, true);
  server.new_xwayland_surface.notify = handle_new_xwayland_surface;
  wl_signal_add(&server.wlr_xwayland->events.new_surface, &server.new_xwayland_surface);

  server.wlr_cursor = wlr_cursor_create();
  wlr_cursor_attach_output_layout(server.wlr_cursor, server.wlr_output_layout);

  server.wlr_xcursor_manager = wlr_xcursor_manager_create(NULL, 24);
  wlr_xcursor_manager_load(server.wlr_xcursor_manager, 1);

  server.cursor_motion.notify = handle_cursor_motion;
  wl_signal_add(&server.wlr_cursor->events.motion, &server.cursor_motion);
  server.cursor_motion_absolute.notify = handle_cursor_motion_absolute;
  wl_signal_add(&server.wlr_cursor->events.motion_absolute, &server.cursor_motion_absolute);
  server.cursor_button.notify = handle_cursor_button;
  wl_signal_add(&server.wlr_cursor->events.button, &server.cursor_button);
  server.cursor_axis.notify = handle_cursor_axis;
  wl_signal_add(&server.wlr_cursor->events.axis, &server.cursor_axis);
  server.cursor_frame.notify = handle_cursor_frame;
  wl_signal_add(&server.wlr_cursor->events.frame, &server.cursor_frame);

  wl_list_init(&server.keyboards);
  server.new_input.notify = handle_new_input;
  wl_signal_add(&server.wlr_backend->events.new_input, &server.new_input);

  server.wlr_seat = wlr_seat_create(server.wl_display, "seat0");
  // TODO: Maybe set up requested cursors?

  wlr_xwayland_set_seat(server.wlr_xwayland, server.wlr_seat);

  const char *socket = wl_display_add_socket_auto(server.wl_display);
  if (!socket) {
    wlr_backend_destroy(server.wlr_backend);
    lua_close(server.lua_state);
    return 1;
  }

  if (!wlr_backend_start(server.wlr_backend)) {
    wlr_backend_destroy(server.wlr_backend);
    wl_display_destroy(server.wl_display);
    lua_close(server.lua_state);
    return 1;
  }

  setenv("WAYLAND_DISPLAY", socket, true);
  setenv("DISPLAY", server.wlr_xwayland->display_name, true);

  wl_display_init_shm(server.wl_display);
  wlr_gamma_control_manager_create(server.wl_display);
  wlr_screencopy_manager_v1_create(server.wl_display);
  wlr_primary_selection_v1_device_manager_create(server.wl_display);
  wlr_idle_create(server.wl_display);

  lua_pushnil(server.lua_state);
  fire_lua_event(&server, EVENT_SERVER_LOADED);

  wlr_log(WLR_INFO, "Running Wayland compositor on WAYLAND_DISPLAY=%s, with Xwayland on DISPLAY=%s", socket, server.wlr_xwayland->display_name);
  wl_display_run(server.wl_display);

  wl_display_destroy_clients(server.wl_display);
  wl_display_destroy(server.wl_display);

  lua_close(server.lua_state);

  return 0;
}