bill.set_log_level(LOG_ERROR)

my_panels = {}

function debug_panel(p)
  local line_out = {}
  for k,v in pairs(p:info()) do
    table.insert(line_out, k .. "=" .. tostring(v))
  end
  print(p, unpack(line_out))
end

function layout_panels()
  local panel_list = bill.list_panels()

  for i,p in pairs(panel_list) do
    p:setpos(0, i * 200)
    p:request_size(500, 200)
  end
end

register_handler(EVENT_NEW_PANEL, function(panel)
  layout_panels()
end)

register_handler(EVENT_PANEL_DESTROY, function(panel)
  layout_panels()
end)

register_binding(KEY_MOD_CTRL + KEY_MOD_SHIFT, "H", true, function()
  local panel_list = bill.list_panels()

  if #panel_list > 0 then
    panel_list[1]:set_hidden(true)
    layout_panels()
  end
end)

register_binding(KEY_MOD_CTRL + KEY_MOD_SHIFT, "S", true, function()
  local panel_list = bill.list_panels()

  for _, p in pairs(panel_list) do
    if p:info().hidden then
      p:set_hidden(false)
    end
  end
  layout_panels()
end)

register_binding(KEY_MOD_CTRL + KEY_MOD_SHIFT, "Return", true, function() bill.fork_exec("urxvt") end)

-- log_key_press = true